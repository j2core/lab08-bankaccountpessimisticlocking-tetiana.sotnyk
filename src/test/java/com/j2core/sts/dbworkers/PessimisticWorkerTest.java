package com.j2core.sts.dbworkers;

import com.j2core.sts.Account;
import com.j2core.sts.CompanyBalance;
import com.j2core.sts.dbworker.PessimisticWorker;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import snaq.db.ConnectionPool;

import java.sql.*;
import java.util.Collection;

/**
 * Created by sts on 7/10/16.
 */
public class PessimisticWorkerTest {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String TABLE_BALANCES = "CREATE TABLE balances ( id INT(11) NOT NULL AUTO_INCREMENT," +
            " company_name TEXT NOT NULL, sum INT(11) NOT NULL, version INT(11) NOT NULL, PRIMARY KEY (id))";
    private static final String TABLE_ACCOUNTS_INFORMATION = "CREATE TABLE accounts_information (" +
            " id INT(11) NOT NULL AUTO_INCREMENT, account_name TEXT NOT NULL, company_name TEXT NOT NULL," +
            " movement_money INT(11) NOT NULL, version INT(11) NOT NULL, PRIMARY KEY (id))";
    private static int counter = 10;


    @BeforeClass
    public void createTestDB(){

        String selectInsertAccount = "INSERT INTO accounts_information (account_name, company_name," +
                " movement_money, version) VALUES(?, 'Macys', 0.00, 1)";

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement();
        PreparedStatement preparedStatement = testConnection.prepareStatement(selectInsertAccount)){

            String sql = "CREATE DATABASE " + NAME_DB + " DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci";
            testStatement.executeUpdate(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.executeUpdate(TABLE_BALANCES);
            testStatement.executeUpdate(TABLE_ACCOUNTS_INFORMATION);

            testStatement.execute("INSERT INTO balances (company_name, sum, version) VALUES('Macys', 100000.00, 1)");

            while (counter > 0){

                String accountName = "account" + counter;
                preparedStatement.setString(1, accountName);

                preparedStatement.execute();

                counter--;
            }

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry!");
        }

    }


    @BeforeGroups(groups = "dbWorker")
    public PessimisticWorker createDBWorker(){

        return new PessimisticWorker();

    }


    @BeforeGroups(groups = "dbWorker")
    public ConnectionPool createConnectionPool() {

        String fullDBUrl = DB_URL + NAME_DB + "?characterEncoding=UTF-8&useSSL=false";
        return new ConnectionPool("St", 30, 100, 250, fullDBUrl, USER, PASS);
    }


    @Test(groups = "dbWorker")
    public void testGetCompanyBalance() throws Exception {

        String companyName = "Macys";

        PessimisticWorker worker = createDBWorker();
        CompanyBalance balance = new CompanyBalance(1, companyName, 100000.00, 1);

        ConnectionPool connectionPool = createConnectionPool();
        CompanyBalance testBalance = worker.getCompanyBalance(connectionPool, companyName);

        Assert.assertTrue(balance.equals(testBalance));

    }


    @Test(groups = "dbWorker")
    public void testGetCompanyAccounts() throws SQLException {

        int amountAccounts = 10;

        PessimisticWorker worker = createDBWorker();
        ConnectionPool connectionPool = createConnectionPool();

        Collection<Account> collection = worker.getCompanyAccounts(connectionPool, "Macys", amountAccounts);

        Assert.assertTrue(collection.size() == amountAccounts);

    }


    @Test(groups = "dbWorker")
    public void testChangeAccountsMovementMoney() throws Exception {

        String accountName = "account1";
        double deltaSum = -10.00;
        PessimisticWorker worker = createDBWorker();
        ConnectionPool connectionPool = createConnectionPool();
        double result = 0.00;

        if (!worker.changeAccountsMovementMoney(connectionPool, accountName,  deltaSum)){
            throw new Exception();
        }

        PreparedStatement statement = connectionPool.getConnection().prepareStatement("SELECT movement_money FROM accounts_information WHERE account_name = ?");
        statement.setString(1, accountName);

        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){

            result = resultSet.getDouble("movement_money");
        }

        Assert.assertEquals(result, deltaSum, 0.001);

    }


    @AfterClass
    public void deleteTestDB() throws SQLException {

        String fullDBUrl = DB_URL + NAME_DB + "?characterEncoding=UTF-8&useSSL=false";

        try (Connection conn = DriverManager.getConnection(fullDBUrl, USER, PASS); Statement stmt = conn.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

}
