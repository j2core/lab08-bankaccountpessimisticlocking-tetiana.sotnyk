package com.j2core.sts.dbworker.exception;

/**
 * Created by sts on 7/10/16.
 */

/**
 * Class exception for DBWorkers
 */
public class DBWorkerException extends Exception{

    public DBWorkerException(String message){
        super();
    }
}
