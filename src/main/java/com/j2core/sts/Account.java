package com.j2core.sts;

/**
 * Created by sts on 7/7/16.
 */

/**
 * Class for save account's name, name company, value movement, version data, and account's id in DB
 */
public class Account {

    private final int id;
    private final String accountName;
    private final String companyName;
    private double movementMoney;
    private int version;

    /**
     * Constructor for Account's class
     *
     * @param id   account's id in DB
     * @param accountName account's name
     * @param companyName account's company name
     * @param movementMoney value account's movement
     * @param version version account's data
     */
    public Account(int id, String accountName, String companyName, double movementMoney, int version) {
        this.id = id;
        this.accountName = accountName;
        this.companyName = companyName;
        this.movementMoney = movementMoney;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public double getMovementMoney() {
        return movementMoney;
    }

    public void setMovementMoney(double deltaMovement) {
        this.movementMoney = movementMoney + deltaMovement;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion() {
        this.version = version++;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Account account = (Account) obj;

        if (id != account.id) return false;
        if (Double.compare(account.movementMoney, movementMoney) != 0) return false;
        if (version != account.version) return false;
        if (!accountName.equals(account.accountName)) return false;
        return companyName.equals(account.companyName);

    }

    @Override
    public int hashCode() {
        int result;
        result = id;
        result = 37 * result + accountName.hashCode();
        result = 37 * result + companyName.hashCode();
        result = 37 * result + version;
        return result;
    }
}
