package com.j2core.sts.dbworker;

import com.j2core.sts.Account;
import com.j2core.sts.CompanyBalance;
import com.j2core.sts.dbworker.exception.DBWorkerException;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by sts on 7/10/16.
 */

/**
 * Class for work with company's balance and account's movement with pessimistic lock DB
 */
public class PessimisticWorker implements DBWorker{

    private static final double INACCURACY = 0.001; // inaccuracy for equals value balances ana movements
    private static final Logger LOGGER = Logger.getLogger(PessimisticWorker.class); //class for save logs information


    /**
     * Constructor PessimisticWorker's class
     */
    public PessimisticWorker() {
    }

    @Override
    public boolean changeBalance(ConnectionPool connectionPool, String companyName, String accountName, double sumChanging) {

        boolean result = false;
        String selectLockAccount = "SELECT movement_money FROM accounts_information WHERE account_name = ? LOCK IN SHARE MODE";
        String selectLockBalance = "SELECT sum FROM balances WHERE company_name = ? LOCK IN SHARE MODE";
        String selectUpdateBalance = "UPDATE balances SET sum = sum + ?, version = version + 1 WHERE company_name = ? ";

        try (Connection connection = connectionPool.getConnection(); PreparedStatement selectLockAccountStatement = connection.prepareStatement(selectLockAccount);
             PreparedStatement selectLockBalanceStatement = connection.prepareStatement(selectLockBalance);
             PreparedStatement updateStatement = connection.prepareStatement(selectUpdateBalance)) {

            selectLockAccountStatement.setString(1, accountName);

            selectLockBalanceStatement.setString(1, companyName);

            updateStatement.setDouble(1, sumChanging);
            updateStatement.setString(2, companyName);

            connection.setAutoCommit(false);

            try (ResultSet resultSet = selectLockBalanceStatement.executeQuery()) {

                double sumBalance = -1;

                if (resultSet.next()) {

                    LOGGER.info("balance lock ");
                    sumBalance = resultSet.getDouble("sum");

                }

                if (Math.abs(sumBalance) <= INACCURACY) throw new DBWorkerException("SORRY! Some thing wrong.");

                if (INACCURACY >= (sumBalance - sumChanging))
                    throw new DBWorkerException("Not enough money in the balance");

                int tmp = updateStatement.executeUpdate();
                if (tmp > 0) {

                    if (changeAccountsMovementMoney(connectionPool, accountName, sumChanging)) {

                        result = true;
                        LOGGER.info("changing Balance");
                    }
                }
            } catch (DBWorkerException e) {
                LOGGER.error(e);
            }
            connection.commit();

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return result;
    }


    /**
     * Method changed value account's movement
     *
     * @param connectionPool   connection for work with DB
     * @param accountName  account's name which value movement need change
     * @param sumChanging  sum changing
     * @return change movement successfully or not
     * @throws SQLException if balance's sum less sum changing
     */
    public boolean changeAccountsMovementMoney(ConnectionPool connectionPool, String accountName, double sumChanging) throws SQLException {

        String selectLockAccount = "SELECT movement_money FROM accounts_information WHERE account_name = ? LOCK IN SHARE MODE";
        String selectUpdateMovementMoney = "UPDATE accounts_information SET movement_money = movement_money + ?, " +
                "version = version + 1 WHERE account_name = ?";
        boolean result = false;

        try(Connection connection = connectionPool.getConnection(); PreparedStatement selectLockStatement = connection.prepareStatement(selectLockAccount);
            PreparedStatement updateStatement = connection.prepareStatement(selectUpdateMovementMoney)) {

            selectLockStatement.setString(1, accountName);

            updateStatement.setDouble(1, sumChanging);
            updateStatement.setString(2, accountName);

            if (updateStatement.executeUpdate() > 0) {
                result = true;
                LOGGER.info("changing account information");
            }

        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return result;

    }


    @Override
    public Collection<Account> getCompanyAccounts(ConnectionPool connectionPool, String companyName, int amountAccounts)
            throws SQLException {

        Collection<Account> accounts = new LinkedList<Account>();
        String selectCompanyAccounts = "SELECT * FROM accounts_information WHERE company_name = ? LOCK IN SHARE MODE";

        try (Connection connection = connectionPool.getConnection(); PreparedStatement statementLock  = connection.prepareStatement(selectCompanyAccounts)){

            connection.setAutoCommit(false);

            statementLock.setString(1, companyName);

            try (ResultSet resultSet = statementLock.executeQuery()) {

                while (resultSet.next()) {

                    accounts.add(new Account(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                            resultSet.getDouble(4), resultSet.getInt(5)));

                }
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return accounts;
    }


    @Override
    public CompanyBalance getCompanyBalance(ConnectionPool connectionPool, String companyName) throws SQLException {

        CompanyBalance companyBalance = null;
        String selectCompanyBalance = "SELECT * FROM balances WHERE company_name = ? LOCK IN SHARE MODE";

        try(Connection connection = connectionPool.getConnection(); PreparedStatement lockStatement = connection.prepareStatement(selectCompanyBalance)) {

            connection.setAutoCommit(false);

            lockStatement.setString(1, companyName);

            try(ResultSet resultSet = lockStatement.executeQuery()) {
                while (resultSet.next()) {

                    companyBalance = new CompanyBalance(resultSet.getInt("id"), resultSet.getString("company_name"),
                            resultSet.getDouble("sum"), resultSet.getInt("version"));
                }
            }
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
        }

        return companyBalance;

    }

}
