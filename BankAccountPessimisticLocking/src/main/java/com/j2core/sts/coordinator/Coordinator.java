package com.j2core.sts.coordinator;

import com.j2core.sts.Account;
import com.j2core.sts.CompanyBalance;
import com.j2core.sts.dbworker.DBWorker;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

import java.sql.SQLException;
import java.util.Collection;

import static java.lang.Thread.sleep;

/**
 * Created by sts on 7/13/16.
 */

/**
 * Class create account's threads for work with balance's data and create company's advance
 */
public class Coordinator implements Runnable {

    public static final Logger LOGGER = Logger.getLogger(Coordinator.class); //class for save logs information
    private final ConnectionPool connectionPool;
    private final Object sync = new Object();
    private final int amountAccounts;
    private final int amountAccountsThreads;
    private final int amountThreadTransactions;
    private final double balanceDecreasing;
    private final DBWorker dbWorker;
    private final String companyName;
    private int nameCounter = 1;
    private ThreadsCounter threadsCounter;

    /**
     * Constructor Coordinator's class
     *
     * @param connectionPool   Connection pool for work with DB
     * @param amountAccounts   amount company's accounts
     * @param amountAccountsThreads  amount account's threads
     * @param amountThreadTransactions  amount thread's transactions
     * @param balanceDecreasing  value balance decreasing
     * @param dbWorker  class for work with DB
     * @param companyName  company's name
     */
    public Coordinator(ConnectionPool connectionPool, int amountAccounts, int amountAccountsThreads, int amountThreadTransactions, double balanceDecreasing, DBWorker dbWorker, String companyName) {
        this.connectionPool = connectionPool;
        this.amountAccounts = amountAccounts;
        this.amountAccountsThreads = amountAccountsThreads;
        this.amountThreadTransactions = amountThreadTransactions;
        this.balanceDecreasing = balanceDecreasing;
        this.dbWorker = dbWorker;
        this.companyName = companyName;
        this.threadsCounter = new ThreadsCounter(amountAccounts * amountAccountsThreads);
    }


    @Override
    public void run() {

        LOGGER.info("Created coordinator thread");
        LOGGER.info("amount threads " + threadsCounter.getAmountTreads());

        for (int i = 0; i < amountAccounts; i++){
            String accountName = "Account" + nameCounter;

            createAndStartedAccountsThreadWorkers(accountName);

            nameCounter++;
        }

        // wait created all account's threads
        try {
            sleep(500);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }

        // woke up all account's threads
        synchronized (sync) {
            sync.notifyAll();
        }

        // wait stopped all account's threads
        synchronized (sync){
            try {
                sync.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // create company's advance
        CompanyBalance companyBalance = null;
        try {
            companyBalance = dbWorker.getCompanyBalance(connectionPool, companyName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collection<Account> accountCollection = null;
        try {
            accountCollection = dbWorker.getCompanyAccounts(connectionPool, companyName, 15);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        LOGGER.info(createAdvanceAccounts(companyBalance, accountCollection));
        LOGGER.info("Stopped thread coordinator");

    }


    /**
     * Method create and star account's thread
     *
     * @param accountName
     */
    public void createAndStartedAccountsThreadWorkers( String accountName){

        int counter = 1;

        while (counter != amountAccountsThreads + 1){

            String nameThread = accountName + "Thread" + counter;
            new Thread(new ThreadWorker(nameThread, dbWorker, amountThreadTransactions, companyName, accountName, balanceDecreasing, sync, threadsCounter, connectionPool)).start();
            counter++;
            LOGGER.info("Create new thread");

        }
    }


    /**
     * Method create company's advance
     *
     * @param companyBalance    company's name
     * @param accountCollection collection with all company's accounts
     * @return  String company's advance
     */
    public String createAdvanceAccounts(CompanyBalance companyBalance, Collection<Account> accountCollection){

        double totalAccountsBalance = 0.00;
        StringBuilder advanceAccounts = new StringBuilder();

        advanceAccounts.append("Balance company ").append(companyName).append(" = ").append(companyBalance.getBalance()).append(System.lineSeparator());
        advanceAccounts.append("Company has ").append(accountCollection.size()).append(" accounts").append(System.lineSeparator());

        for (Account account: accountCollection){

            advanceAccounts.append("Account ").append(account.getAccountName()).append(" has balance = ").append(account.getMovementMoney()).append(System.lineSeparator());
            totalAccountsBalance = totalAccountsBalance + account.getMovementMoney();

        }

        advanceAccounts.append("Total balance all accounts = ").append(totalAccountsBalance).append(System.lineSeparator());

        return advanceAccounts.toString();

    }

}
